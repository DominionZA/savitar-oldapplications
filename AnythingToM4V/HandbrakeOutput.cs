﻿using System;

namespace AnythingToM4V
{
    public class HandbrakeOutput
    {
        public float PercentComplete { get; private set; }
        public float Fps { get; private set; }
        public float AverageFps { get; private set; }
        public TimeSpan Eta { get; set; }

        public override string ToString()
        {
            return $"{PercentComplete:#00.00}% Complete, FPS: {Fps}, Avg FPS: {AverageFps}, ETA: {Eta}";
        }

        public static HandbrakeOutput Get(string data)
        {
            if (string.IsNullOrEmpty(data))
                return null;

            return !data.StartsWith("Encoding") ? null : new HandbrakeOutput(data);
        }

        internal HandbrakeOutput(string data)
        {
            // Encoding: task 1 of 1, 0.13 %
            // Encoding: task 1 of 1, 1.77 % (147.33 fps, avg 149.55 fps, ETA 00h07m13s)

            try
            {
                var openBracketIndex = data.IndexOf('(');
                if (openBracketIndex >= 0)
                {
                    var extras = data.Substring(openBracketIndex + 1);
                    extras = extras.Replace(")", "");

                    var s = extras.Split(',');
                    Fps = float.Parse(s[0].Replace("fps", ""));
                    AverageFps = float.Parse(s[1].Replace("fps", "").Replace("avg", ""));

                    var eta = s[2].Replace("ETA", "").Trim();
                    Eta = new TimeSpan(int.Parse(eta.Substring(0, 2)), int.Parse(eta.Substring(3, 2)),
                        int.Parse(eta.Substring(6, 2)));
                }

                var commaIndex = data.IndexOf(',');
                if (commaIndex < 0)
                    return;

                data = data.Substring(commaIndex + 1);
                var percentIndex = data.IndexOf('%');

                PercentComplete = float.Parse(data.Substring(0, percentIndex));
            }
            catch (Exception)
            {
                // Silent for now. When not feeling too lazy implement some error checking in the above code.
            }
        }
    }
}
