﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AnythingToM4V
{
    public partial class MainForm : Form
    {
        private readonly string title;

        private readonly Handbrake handbrake = new Handbrake();
        private Settings settings;        
        public MainForm()
        {
            InitializeComponent();

            title = Text;
            LoadSettings();

            HandbrakeProgress.Minimum = 0;
            HandbrakeProgress.Maximum = 100;
            handbrake.OnStatus += handbrake_OnStatus;
        }

        public sealed override string Text { get; set; }

        void handbrake_OnStatus(HandbrakeOutput output)
        {
            if (outputLabel.InvokeRequired)
                outputLabel.Invoke(new MethodInvoker(delegate { outputLabel.Text = output.ToString(); }));
            if (HandbrakeProgress.InvokeRequired)
                HandbrakeProgress.Invoke(new MethodInvoker(delegate { HandbrakeProgress.Value = (int)output.PercentComplete; }));
        }

        protected IList<FileInfo> GetFiles(string rootFolder)
        {
            var files = Savitar.IO.File.Search(rootFolder, settings.SourceFileMask);
            return files.Select(file => new FileInfo(file)).Where(fileInfo => fileInfo.Length > 0).ToList();
        }

        private void ProcessButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(settings.PathToHandbrakeCli))
                throw new Exception("Path to Handbrake CLI has not been set");
            if (!File.Exists(settings.PathToHandbrakeCli))
                throw new Exception("The file " + settings.PathToHandbrakeCli + " does not exist");

            handbrake.PathToHandbrakeCli = settings.PathToHandbrakeCli;

            var backgroundWorker = new BackgroundWorker {WorkerReportsProgress = true, WorkerSupportsCancellation = true};
            backgroundWorker.DoWork += worker_DoWork;
            backgroundWorker.ProgressChanged += worker_ProgressChanged;
            backgroundWorker.RunWorkerCompleted += worker_RunWorkerCompleted;
            backgroundWorker.RunWorkerAsync();            
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            if (worker == null)
            {
                e.Cancel = true;
                return;
            }

            var sourceFiles = new List<FileInfo>();

            foreach (var sourceFolder in settings.SourceFolders)
            {
                sourceFiles.AddRange(GetFiles(sourceFolder));                    
            }

            var processedCount = 0;
            
            if (InvokeRequired)
                Invoke(new MethodInvoker(delegate { Text = $"{title} {processedCount} of {sourceFiles.Count}"; }));

            foreach (var sourceFile in sourceFiles)
            {
                var destinationFile = Path.GetDirectoryName(sourceFile.FullName) + "\\" + Path.GetFileNameWithoutExtension(sourceFile.FullName) + ".m4v";

                if (CurrentFileTextBox.InvokeRequired)
                    Invoke(new MethodInvoker(delegate { CurrentFileTextBox.Text = sourceFile.FullName; }));            

                handbrake.ProcessFile(sourceFile.FullName, destinationFile, settings.HandbrakePreset);

                if (handbrake.WaitUntilComplete() == 0)
                    DoPostProcessing(sourceFile, new FileInfo(destinationFile));

                processedCount++;
            }
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            handbrake.Stop();
            outputLabel.Text = @"Completed";
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {                  
        }        

        protected void DoPostProcessing(FileInfo sourceFile, FileInfo destinationFile)
        {
            if (settings.DeleteSourceFile)
                File.Delete(sourceFile.FullName);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            handbrake?.Dispose();
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            handbrake?.Stop();
        }

        private void SourceFoldersButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK)
                return;

            SourceFoldersListBox.Items.Add(folderBrowserDialog.SelectedPath);
            SaveSettings();
        }

        protected void LoadSettings()
        {
            if (settings == null)
                settings = Settings.Load();

            PathToHandbrakeCLILabel.Text = settings.PathToHandbrakeCli;
            HandbrakePresetTextBox.Text = settings.HandbrakePreset;
            foreach (string item in settings.SourceFolders)
               SourceFoldersListBox.Items.Add(item);
            SourceFileMaskTextBox.Text = settings.SourceFileMask;
            DeleteSourceFileCheckBox.Checked = settings.DeleteSourceFile;
        }

        protected void SaveSettings()
        {
            settings.PathToHandbrakeCli = PathToHandbrakeCLILabel.Text;
            settings.HandbrakePreset = HandbrakePresetTextBox.Text;

            settings.SourceFolders.Clear();
            foreach (string item in SourceFoldersListBox.Items)
            {
                settings.SourceFolders.Add(item);
            }
            settings.SourceFileMask = SourceFileMaskTextBox.Text;
            settings.DeleteSourceFile = DeleteSourceFileCheckBox.Checked;
            settings.Save();
        }

        private void SourceFoldersDelete_Click(object sender, EventArgs e)
        {            
            for (int i = SourceFoldersListBox.Items.Count - 1; i == 0; i--)
            {                
                if (SourceFoldersListBox.GetSelected(i))
                    SourceFoldersListBox.Items.RemoveAt(i);
            }
        }

        private void PathToHandbrakeCLIButton_Click(object sender, EventArgs e)
        {            
            openFileDialog.FileName = settings.PathToHandbrakeCli;
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            PathToHandbrakeCLILabel.Text = openFileDialog.FileName;
           
            SaveSettings();
        }

        private void TextBoxLeave(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void DeleteSourceFileCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SaveSettings();
        }

        //void process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        //{
        //}        
    }
}
