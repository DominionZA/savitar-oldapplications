﻿using System;
using System.Collections.Generic;

namespace AnythingToM4V
{
    [Serializable]
    public class Settings
    {
        public string PathToHandbrakeCli { get; set; }
        public string HandbrakePreset
        {
            get => string.IsNullOrEmpty(handbrakePreset) ? "AppleTV 3" : handbrakePreset;
            set => handbrakePreset = value;
        }

        private string handbrakePreset;

        public string SourceFileMask
        {
            get => string.IsNullOrEmpty(sourceFileMask) ? "*.mkv;*.avi" : sourceFileMask;
            set => sourceFileMask = value;
        }

        private string sourceFileMask;

        public string SourceFileBackupRoot { get; set; }
        public bool DeleteSourceFile { get; set; }
        public List<string> SourceFolders
        {
            get => sourceFolders ?? (sourceFolders = new List<string>());
            set => sourceFolders = value;
        }

        private List<string> sourceFolders;

        public static Settings Load()
        {
            return Savitar.Object.Load<Settings>("Settings.xml") ?? new Settings();
        }

        public void Save()
        {
            Savitar.Object.Save("Settings.xml", this);
        }
    }
}