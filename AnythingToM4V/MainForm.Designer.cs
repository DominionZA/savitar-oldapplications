﻿namespace AnythingToM4V
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProcessButton = new System.Windows.Forms.Button();
            this.outputLabel = new System.Windows.Forms.Label();
            this.HandbrakeProgress = new System.Windows.Forms.ProgressBar();
            this.StopButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SourceFileMaskTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SourceFoldersDelete = new System.Windows.Forms.Button();
            this.SourceFoldersButton = new System.Windows.Forms.Button();
            this.SourceFoldersListBox = new System.Windows.Forms.ListBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.HandbrakePresetTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PathToHandbrakeCLIButton = new System.Windows.Forms.Button();
            this.PathToHandbrakeCLILabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DeleteSourceFileCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BackupSourceFileRootPath = new System.Windows.Forms.Button();
            this.Label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CurrentFileTextBox = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProcessButton
            // 
            this.ProcessButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ProcessButton.Location = new System.Drawing.Point(571, 351);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(75, 23);
            this.ProcessButton.TabIndex = 0;
            this.ProcessButton.Text = "Process";
            this.ProcessButton.UseVisualStyleBackColor = true;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // outputLabel
            // 
            this.outputLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputLabel.Location = new System.Drawing.Point(12, 322);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(553, 23);
            this.outputLabel.TabIndex = 1;
            this.outputLabel.Text = "A2A Status";
            // 
            // HandbrakeProgress
            // 
            this.HandbrakeProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HandbrakeProgress.Location = new System.Drawing.Point(12, 293);
            this.HandbrakeProgress.Name = "HandbrakeProgress";
            this.HandbrakeProgress.Size = new System.Drawing.Size(634, 23);
            this.HandbrakeProgress.TabIndex = 3;
            // 
            // StopButton
            // 
            this.StopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StopButton.Location = new System.Drawing.Point(571, 322);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(75, 23);
            this.StopButton.TabIndex = 4;
            this.StopButton.Text = "Stop";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.SourceFileMaskTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SourceFoldersDelete);
            this.groupBox1.Controls.Add(this.SourceFoldersButton);
            this.groupBox1.Controls.Add(this.SourceFoldersListBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 129);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Source Folders";
            // 
            // SourceFileMaskTextBox
            // 
            this.SourceFileMaskTextBox.Location = new System.Drawing.Point(160, 94);
            this.SourceFileMaskTextBox.Name = "SourceFileMaskTextBox";
            this.SourceFileMaskTextBox.Size = new System.Drawing.Size(113, 20);
            this.SourceFileMaskTextBox.TabIndex = 8;
            this.SourceFileMaskTextBox.Leave += new System.EventHandler(this.TextBoxLeave);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "File Mask";
            // 
            // SourceFoldersDelete
            // 
            this.SourceFoldersDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SourceFoldersDelete.Location = new System.Drawing.Point(590, 19);
            this.SourceFoldersDelete.Name = "SourceFoldersDelete";
            this.SourceFoldersDelete.Size = new System.Drawing.Size(37, 23);
            this.SourceFoldersDelete.TabIndex = 2;
            this.SourceFoldersDelete.Text = "D";
            this.SourceFoldersDelete.UseVisualStyleBackColor = true;
            this.SourceFoldersDelete.Click += new System.EventHandler(this.SourceFoldersDelete_Click);
            // 
            // SourceFoldersButton
            // 
            this.SourceFoldersButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SourceFoldersButton.Location = new System.Drawing.Point(590, 65);
            this.SourceFoldersButton.Name = "SourceFoldersButton";
            this.SourceFoldersButton.Size = new System.Drawing.Size(37, 23);
            this.SourceFoldersButton.TabIndex = 1;
            this.SourceFoldersButton.Text = "...";
            this.SourceFoldersButton.UseVisualStyleBackColor = true;
            this.SourceFoldersButton.Click += new System.EventHandler(this.SourceFoldersButton_Click);
            // 
            // SourceFoldersListBox
            // 
            this.SourceFoldersListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SourceFoldersListBox.FormattingEnabled = true;
            this.SourceFoldersListBox.Location = new System.Drawing.Point(6, 19);
            this.SourceFoldersListBox.Name = "SourceFoldersListBox";
            this.SourceFoldersListBox.Size = new System.Drawing.Size(578, 69);
            this.SourceFoldersListBox.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.HandbrakePresetTextBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.PathToHandbrakeCLIButton);
            this.groupBox2.Controls.Add(this.PathToHandbrakeCLILabel);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 147);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(634, 66);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Handbrake Settings";
            // 
            // HandbrakePresetTextBox
            // 
            this.HandbrakePresetTextBox.Location = new System.Drawing.Point(160, 36);
            this.HandbrakePresetTextBox.Name = "HandbrakePresetTextBox";
            this.HandbrakePresetTextBox.Size = new System.Drawing.Size(113, 20);
            this.HandbrakePresetTextBox.TabIndex = 6;
            this.HandbrakePresetTextBox.Leave += new System.EventHandler(this.TextBoxLeave);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Handbrake Preset";
            // 
            // PathToHandbrakeCLIButton
            // 
            this.PathToHandbrakeCLIButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PathToHandbrakeCLIButton.Location = new System.Drawing.Point(590, 11);
            this.PathToHandbrakeCLIButton.Name = "PathToHandbrakeCLIButton";
            this.PathToHandbrakeCLIButton.Size = new System.Drawing.Size(37, 23);
            this.PathToHandbrakeCLIButton.TabIndex = 4;
            this.PathToHandbrakeCLIButton.Text = "...";
            this.PathToHandbrakeCLIButton.UseVisualStyleBackColor = true;
            this.PathToHandbrakeCLIButton.Click += new System.EventHandler(this.PathToHandbrakeCLIButton_Click);
            // 
            // PathToHandbrakeCLILabel
            // 
            this.PathToHandbrakeCLILabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PathToHandbrakeCLILabel.Location = new System.Drawing.Point(157, 16);
            this.PathToHandbrakeCLILabel.Name = "PathToHandbrakeCLILabel";
            this.PathToHandbrakeCLILabel.Size = new System.Drawing.Size(427, 23);
            this.PathToHandbrakeCLILabel.TabIndex = 3;
            this.PathToHandbrakeCLILabel.Text = "Path to HandbrakeCLI.exe";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Path to HandbrakeCLI.exe";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.DeleteSourceFileCheckBox);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.BackupSourceFileRootPath);
            this.groupBox3.Controls.Add(this.Label10);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(12, 219);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(634, 68);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Post Processing Settings";
            // 
            // DeleteSourceFileCheckBox
            // 
            this.DeleteSourceFileCheckBox.AutoSize = true;
            this.DeleteSourceFileCheckBox.Location = new System.Drawing.Point(160, 38);
            this.DeleteSourceFileCheckBox.Name = "DeleteSourceFileCheckBox";
            this.DeleteSourceFileCheckBox.Size = new System.Drawing.Size(15, 14);
            this.DeleteSourceFileCheckBox.TabIndex = 7;
            this.DeleteSourceFileCheckBox.UseVisualStyleBackColor = true;
            this.DeleteSourceFileCheckBox.CheckedChanged += new System.EventHandler(this.DeleteSourceFileCheckBox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(157, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(427, 23);
            this.label4.TabIndex = 6;
            this.label4.Text = "Path to HandbrakeCLI.exe";
            // 
            // BackupSourceFileRootPath
            // 
            this.BackupSourceFileRootPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BackupSourceFileRootPath.Location = new System.Drawing.Point(590, 11);
            this.BackupSourceFileRootPath.Name = "BackupSourceFileRootPath";
            this.BackupSourceFileRootPath.Size = new System.Drawing.Size(37, 23);
            this.BackupSourceFileRootPath.TabIndex = 5;
            this.BackupSourceFileRootPath.Text = "...";
            this.BackupSourceFileRootPath.UseVisualStyleBackColor = true;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(6, 16);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(150, 23);
            this.Label10.TabIndex = 3;
            this.Label10.Text = "Backup source file";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Delete source file when done";
            // 
            // CurrentFileTextBox
            // 
            this.CurrentFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CurrentFileTextBox.Location = new System.Drawing.Point(12, 351);
            this.CurrentFileTextBox.Name = "CurrentFileTextBox";
            this.CurrentFileTextBox.Size = new System.Drawing.Size(553, 23);
            this.CurrentFileTextBox.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 383);
            this.Controls.Add(this.CurrentFileTextBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.HandbrakeProgress);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.ProcessButton);
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Anything 2 Anything Converter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.Label outputLabel;
        private System.Windows.Forms.ProgressBar HandbrakeProgress;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox SourceFoldersListBox;
        private System.Windows.Forms.Button SourceFoldersButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button SourceFoldersDelete;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button PathToHandbrakeCLIButton;
        private System.Windows.Forms.Label PathToHandbrakeCLILabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox HandbrakePresetTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SourceFileMaskTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button BackupSourceFileRootPath;
        private System.Windows.Forms.Label Label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox DeleteSourceFileCheckBox;
        private System.Windows.Forms.Label CurrentFileTextBox;
    }
}

