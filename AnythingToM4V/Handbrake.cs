﻿using System;
using System.Diagnostics;

namespace AnythingToM4V
{
    public class Handbrake : IDisposable
    {
        private Process Process { get; set; }
        public string PathToHandbrakeCli { get; set; }

        public delegate void OnStatusDelegate(HandbrakeOutput output);
        public event OnStatusDelegate OnStatus;

        protected virtual void HandleOnStatus(HandbrakeOutput output)
        {
            OnStatus?.Invoke(output);
        }

        public void ProcessFile(string sourceFile, string destinationFile, string preset)
        {
            var handbrakeArgs = $"-i {sourceFile} -o {destinationFile} --preset={preset}";

            Process = new Process
            {
                StartInfo =
                                  {
                                      FileName = PathToHandbrakeCli,
                                      Arguments = handbrakeArgs,
                                      CreateNoWindow = true,
                                      UseShellExecute = false,
                                      RedirectStandardOutput = true
                                  }
            };

            Process.OutputDataReceived += OutputDataReceived;

            Process.Start();
            Process.BeginOutputReadLine();
        }

        public int WaitUntilComplete()
        {
            if (Process.HasExited)
                return -1;

            Process.WaitForExit();

            return Process.ExitCode;
        }

        private void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                var output = HandbrakeOutput.Get(e.Data);
                if (output == null)
                    return;

                HandleOnStatus(output);
            }
        }

        public bool IsBusy
        {
            get
            {
                if (Process == null)
                    return false;

                return !Process.HasExited;
            }
        }

        public void Stop()
        {
            if (IsBusy)
                Process.Kill();
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
