﻿namespace Plex_Exporter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.PlexHostTextBox = new System.Windows.Forms.TextBox();
            this.PlexHostPortTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SectionsComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SectionOptionsComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ExportButton = new System.Windows.Forms.Button();
            this.ItemsListBox = new System.Windows.Forms.ListBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Plex Host";
            // 
            // PlexHostTextBox
            // 
            this.PlexHostTextBox.Location = new System.Drawing.Point(90, 18);
            this.PlexHostTextBox.Name = "PlexHostTextBox";
            this.PlexHostTextBox.Size = new System.Drawing.Size(172, 20);
            this.PlexHostTextBox.TabIndex = 1;
            this.PlexHostTextBox.Text = "192.168.1.2";
            // 
            // PlexHostPortTextBox
            // 
            this.PlexHostPortTextBox.Location = new System.Drawing.Point(353, 18);
            this.PlexHostPortTextBox.Name = "PlexHostPortTextBox";
            this.PlexHostPortTextBox.Size = new System.Drawing.Size(68, 20);
            this.PlexHostPortTextBox.TabIndex = 3;
            this.PlexHostPortTextBox.Text = "32400";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(275, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Plex Host Port";
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(427, 16);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(75, 23);
            this.RefreshButton.TabIndex = 4;
            this.RefreshButton.Text = "Refresh";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Section";
            // 
            // SectionsComboBox
            // 
            this.SectionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SectionsComboBox.FormattingEnabled = true;
            this.SectionsComboBox.Location = new System.Drawing.Point(90, 78);
            this.SectionsComboBox.Name = "SectionsComboBox";
            this.SectionsComboBox.Size = new System.Drawing.Size(412, 21);
            this.SectionsComboBox.TabIndex = 6;
            this.SectionsComboBox.SelectedIndexChanged += new System.EventHandler(this.SectionsComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Option";
            // 
            // SectionOptionsComboBox
            // 
            this.SectionOptionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SectionOptionsComboBox.FormattingEnabled = true;
            this.SectionOptionsComboBox.Location = new System.Drawing.Point(90, 105);
            this.SectionOptionsComboBox.Name = "SectionOptionsComboBox";
            this.SectionOptionsComboBox.Size = new System.Drawing.Size(259, 21);
            this.SectionOptionsComboBox.TabIndex = 8;
            this.SectionOptionsComboBox.SelectedIndexChanged += new System.EventHandler(this.SectionOptionsComboBox_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Section";
            // 
            // ExportButton
            // 
            this.ExportButton.Location = new System.Drawing.Point(427, 375);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(75, 23);
            this.ExportButton.TabIndex = 11;
            this.ExportButton.Text = "Export";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // ItemsListBox
            // 
            this.ItemsListBox.FormattingEnabled = true;
            this.ItemsListBox.Location = new System.Drawing.Point(90, 155);
            this.ItemsListBox.Name = "ItemsListBox";
            this.ItemsListBox.Size = new System.Drawing.Size(412, 212);
            this.ItemsListBox.TabIndex = 12;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 404);
            this.Controls.Add(this.ItemsListBox);
            this.Controls.Add(this.ExportButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.SectionOptionsComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SectionsComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.PlexHostPortTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PlexHostTextBox);
            this.Controls.Add(this.label1);
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Plex Exporter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PlexHostTextBox;
        private System.Windows.Forms.TextBox PlexHostPortTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox SectionsComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox SectionOptionsComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ExportButton;
        private System.Windows.Forms.ListBox ItemsListBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

