﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Savitar.Plex;
using Savitar.Plex.Model;
using Savitar.Plex.Urls;

namespace Plex_Exporter
{
    public partial class MainForm : Form
    {
        private PlexServer server;
        public MainForm()
        {
            InitializeComponent();
            UpdateUi();
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(PlexHostPortTextBox.Text, out var port))
                port = 32400;

            server = new PlexServer(PlexHostTextBox.Text, port);

            ListSections();
        }

        protected bool VerifyServer()
        {
            if (server != null) 
                return true;

            MessageBox.Show(@"Please populate the host and port fields and click Connect to establish a connection with your Plex server",
                @"Connect To Plex Server", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            return false;
        }

        protected void ListSections()
        {
            if (!VerifyServer())
                return;

            try
            {
                Cursor = Cursors.WaitCursor;
                ItemsListBox.DataSource = null;
                SectionOptionsComboBox.DataSource = null;
                SectionsComboBox.DataSource = null;

                var sections = server.GetLibrarySections();
                if (sections == null)
                {
                    MessageBox.Show(@"No sections returned", @"No Sections", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                SectionsComboBox.DataSource = sections;
                if (SectionOptionsComboBox.Items.Count > 0)
                    SectionsComboBox.SelectedIndex = 0;
            }
            finally
            {
                UpdateUi();
                Cursor = Cursors.Default;
            }
        }

        private void SectionsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SectionOptionsComboBox.DataSource = null;

            if (!(SectionsComboBox.SelectedItem is Section section))
                return;

            try
            {
                Cursor = Cursors.WaitCursor;
                MediaContainer mediaContainer = server.GetMediaContainer(section.Path);

                SectionOptionsComboBox.DataSource =
                    mediaContainer.Directories.Where(x => x.Secondary == 0 && x.Search == 0 && x.Key != "folder").ToList();
                if (SectionOptionsComboBox.Items.Count > 0)
                    SectionOptionsComboBox.SelectedIndex = 0;
            }
            finally
            {
                UpdateUi();
                Cursor = Cursors.Default;
            }
        }

        private void SectionOptionsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemsListBox.DataSource = null;

            if (SectionsComboBox.DataSource == null)
                return;
            if (SectionOptionsComboBox.DataSource == null)
                return;

            var section = SectionsComboBox.SelectedItem as Section;
            var sectionOption = SectionOptionsComboBox.SelectedItem as Directory;

            if (section == null || sectionOption == null)
                return;

            try
            {
                Cursor = Cursors.WaitCursor;
                MediaContainer mediaContainer = server.GetSectionOptionList(section, sectionOption);

                if (mediaContainer.Directories.Count > 0)
                    ItemsListBox.DataSource = mediaContainer.Directories.OrderBy(x => x.Title).ToList();
                else
                    ItemsListBox.DataSource = mediaContainer.Videos.OrderBy(x => x.Title).ToList();
            }
            finally
            {
                UpdateUi();
                Cursor = Cursors.Default;
            }
        }

        protected void UpdateUi()
        {
            ExportButton.Enabled = ItemsListBox.Items.Count > 0;
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = @"HTML files (*.html)|*.html";
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
                return;

            IList<IPlexItem> plexItems = (from object item in ItemsListBox.Items select item as IPlexItem).ToList();

            WriteHtml(saveFileDialog.FileName, SectionsComboBox.SelectedItem + " - " + SectionOptionsComboBox.SelectedItem, plexItems);
        }

        protected void WriteHtml(string filename, string pageTitle, IList<IPlexItem> data)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.AppendLine("<head>");
            sb.AppendLine("<meta charset=\"utf-8\" />");
            sb.AppendLine("<title>" + pageTitle + "</title>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("<table>");

            sb.AppendLine("<tr>");
            sb.AppendLine("<td style=\"width:350px; vertical-align:top;border-bottom-style: solid; border-bottom-width: 1px; font-weight: bold\">Title</td>");
            sb.AppendLine("<td style=\"width:50px; vertical-align:top;border-bottom-style: solid; border-bottom-width: 1px; font-weight: bold\">Year</td>");
            sb.AppendLine("<td style=\"vertical-align:top;border-bottom-style: solid; border-bottom-width: 1px; font-weight: bold\">Summary</td>");
            sb.AppendLine("</tr>");

            foreach (IPlexItem plexItem in data)
            {
                sb.AppendLine(GetHtmlRow(plexItem));
            }

            sb.AppendLine("</table>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");

            System.IO.File.WriteAllText(filename, sb.ToString());
        }

        protected string GetHtmlRow(IPlexItem plexItem)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<tr>");

            sb.Append("<td style=\"border-bottom-style: solid; border-bottom-width: 1px;\">");
            sb.Append(plexItem.Title);
            sb.AppendLine("</td>");

            sb.Append("<td style=\"border-bottom-style: solid; border-bottom-width: 1px;\">");
            sb.Append(plexItem.Year);
            sb.AppendLine("</td>");

            sb.Append("<td style=\"border-bottom-style: solid; border-bottom-width: 1px;\">");
            sb.Append(plexItem.Summary);
            sb.AppendLine("</td>");

            sb.AppendLine("</tr>");
            return sb.ToString();
        }
    }
}
