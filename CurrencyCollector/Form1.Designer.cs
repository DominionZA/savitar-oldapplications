﻿namespace CurrencyCollector
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LastUpdateLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RateLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.FromCurrencyTextBox = new System.Windows.Forms.TextBox();
            this.ToCurrencyTextBox = new System.Windows.Forms.TextBox();
            this.TestButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.DBValueLabel = new System.Windows.Forms.Label();
            this.DBConnectionString = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(12, 283);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(75, 23);
            this.UpdateButton.TabIndex = 0;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Last Update";
            // 
            // LastUpdateLabel
            // 
            this.LastUpdateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LastUpdateLabel.Location = new System.Drawing.Point(106, 139);
            this.LastUpdateLabel.Name = "LastUpdateLabel";
            this.LastUpdateLabel.Size = new System.Drawing.Size(288, 13);
            this.LastUpdateLabel.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rate";
            // 
            // RateLabel
            // 
            this.RateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RateLabel.Location = new System.Drawing.Point(106, 161);
            this.RateLabel.Name = "RateLabel";
            this.RateLabel.Size = new System.Drawing.Size(288, 13);
            this.RateLabel.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Status";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusLabel.Location = new System.Drawing.Point(106, 205);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(288, 56);
            this.StatusLabel.TabIndex = 6;
            // 
            // timer
            // 
            this.timer.Interval = 3600000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "From Currency";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "To Currency";
            // 
            // FromCurrencyTextBox
            // 
            this.FromCurrencyTextBox.Location = new System.Drawing.Point(106, 12);
            this.FromCurrencyTextBox.Name = "FromCurrencyTextBox";
            this.FromCurrencyTextBox.Size = new System.Drawing.Size(100, 20);
            this.FromCurrencyTextBox.TabIndex = 10;
            this.FromCurrencyTextBox.Text = "ZAR";
            // 
            // ToCurrencyTextBox
            // 
            this.ToCurrencyTextBox.Location = new System.Drawing.Point(106, 38);
            this.ToCurrencyTextBox.Name = "ToCurrencyTextBox";
            this.ToCurrencyTextBox.Size = new System.Drawing.Size(100, 20);
            this.ToCurrencyTextBox.TabIndex = 11;
            this.ToCurrencyTextBox.Text = "USD";
            // 
            // TestButton
            // 
            this.TestButton.Location = new System.Drawing.Point(212, 36);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(41, 23);
            this.TestButton.TabIndex = 12;
            this.TestButton.Text = "Test";
            this.TestButton.UseVisualStyleBackColor = true;
            this.TestButton.Click += new System.EventHandler(this.TestButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "DB Value";
            // 
            // DBValueLabel
            // 
            this.DBValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DBValueLabel.Location = new System.Drawing.Point(106, 183);
            this.DBValueLabel.Name = "DBValueLabel";
            this.DBValueLabel.Size = new System.Drawing.Size(288, 13);
            this.DBValueLabel.TabIndex = 14;
            // 
            // DBConnectionString
            // 
            this.DBConnectionString.Location = new System.Drawing.Point(106, 64);
            this.DBConnectionString.Name = "DBConnectionString";
            this.DBConnectionString.Size = new System.Drawing.Size(288, 20);
            this.DBConnectionString.TabIndex = 15;
            this.DBConnectionString.Text = "Data Source=winsqls01.cpt.wa.co.za;Initial Catalog=SAV_Vaping101;User ID=msmit;Password=Sundio1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Connection String";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 317);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.DBConnectionString);
            this.Controls.Add(this.DBValueLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TestButton);
            this.Controls.Add(this.ToCurrencyTextBox);
            this.Controls.Add(this.FromCurrencyTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RateLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LastUpdateLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UpdateButton);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "V101 Currency Updater";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LastUpdateLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label RateLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox FromCurrencyTextBox;
        private System.Windows.Forms.TextBox ToCurrencyTextBox;
        private System.Windows.Forms.Button TestButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label DBValueLabel;
        private System.Windows.Forms.TextBox DBConnectionString;
        private System.Windows.Forms.Label label7;
    }
}

