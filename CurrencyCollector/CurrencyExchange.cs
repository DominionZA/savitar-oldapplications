﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace CurrencyCollector
{
    public class CurrencyExchange
    {
        public string FromCurrency { get; private set; }
        public string ToCurrency { get; private set; }

        // Have to factor in 5% for currency purchase and PayPal fees (3.4% + $0.30 per transaction).
        public double ExchangeRate
        {
            get => exchangeRate / 100 * 95;
            private set => exchangeRate = value;
        }

        private double exchangeRate;
        public double InverseExchangeRate { get; private set; }

        public string ExchangeRateStr => $"{FromCurrency} 1 = {ToCurrency} {ExchangeRate:###,###,##0.00}";

        public string InverseExchangeRateStr => $"{ToCurrency} 1 = {FromCurrency} {InverseExchangeRate:###,###,##0.00}";

        public CurrencyExchange(string fromCurrency, string toCurrency)
        {
            FromCurrency = fromCurrency.ToUpper();
            ToCurrency = toCurrency.ToUpper();            
        }

        public void Refresh()
        {
            const decimal amount = 1;
            //Grab your values and build your Web Request to the API
            var apiUrl = $"https://www.google.com/finance/converter?a={amount}&from={FromCurrency}&to={ToCurrency}&meta={Guid.NewGuid()}";

            //Make your Web Request and grab the results
            var request = WebRequest.Create(apiUrl);

            //Get the Response
            var streamReader = new StreamReader(request.GetResponse().GetResponseStream(), System.Text.Encoding.ASCII);

            //Grab your converted value (ie 2.45 GPD)
            var result = Regex.Matches(streamReader.ReadToEnd(), "<span class=\"?bld\"?>([^<]+)</span>")[0].Groups[1].Value;
            result = result.Replace(" " + ToCurrency, "");

            if (!float.TryParse(result, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out var value))
                throw new Exception("Unable to parse the result [" + result + "] to a double");

            ExchangeRate = value;

            if (Math.Abs(ExchangeRate) <= 0)
                InverseExchangeRate = 0;
            else
                InverseExchangeRate = 1 / ExchangeRate;
        }

        //public void Refresh()
        //{
        //    decimal amount = 1;
            
        //    WebClient web = new WebClient();
            
        //    string url = string.Format("http://www.google.com/ig/calculator?hl=en&q={2}{0}%3D%3F{1}", FromCurrency, ToCurrency, amount);
        //    string response = web.DownloadString(url);

        //    Regex regex = new Regex("rhs: \\\"(\\d*.\\d*)");
        //    Match match = regex.Match(response);

        //    ExchangeRate = Convert.ToDouble(match.Groups[1].Value);

        //    if (ExchangeRate == 0)
        //        InverseExchangeRate = 0;
        //    else
        //        InverseExchangeRate = 1/ExchangeRate;
        //}

        public override string ToString()
        {
            return ExchangeRateStr + ", " + InverseExchangeRateStr;
        }
    }
}
