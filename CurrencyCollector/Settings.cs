﻿using System.Data.EntityClient;

namespace CurrencyCollector
{
    public class Settings
    {
        public string FromCurrencyCode { get; set; }
        public string ToCurrencyCode { get; set; }
        public string DatabaseConnectionString
        {
            get => databaseConnectionString;
            set
            {
                databaseConnectionString = value;

                var entityBuilder = new EntityConnectionStringBuilder
                {
                    Provider = "System.Data.SqlClient",
                    ProviderConnectionString = databaseConnectionString,
                    Metadata = "res://*/V101Model.csdl|res://*/V101Model.ssdl|res://*/V101Model.msl"
                };
                EntityConnectionString = entityBuilder.ToString();
            }
        }

        private string databaseConnectionString;

        public string EntityConnectionString { get; set; }

        public static Settings Instance =>
            _instance ?? (_instance =
                (Settings) Savitar.Object.Load("Settings.dat", typeof(Settings)) ?? new Settings());

        private static Settings _instance;

        private Settings()
        {
        }

        public void Save()
        {
            Savitar.Object.Save("Settings.dat", this);
        }
    }
}