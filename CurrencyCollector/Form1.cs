﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace CurrencyCollector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

#if DEBUG
            timer.Interval = 3600000; // 1 Hour
#else
            timer.Interval = 3600000;
#endif

            timer.Enabled = true;
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            DoUpdate();
        }

        protected void DoUpdate()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    Settings.Instance.FromCurrencyCode = FromCurrencyTextBox.Text;
                    Settings.Instance.ToCurrencyCode = ToCurrencyTextBox.Text;
                    Settings.Instance.DatabaseConnectionString = DBConnectionString.Text;

                    var exchange = new CurrencyExchange(FromCurrencyTextBox.Text, ToCurrencyTextBox.Text);
                    exchange.Refresh();

                    using (var entities = new SAV_Vaping101Entities(Settings.Instance.EntityConnectionString))
                    {
                        var storeCurrency = entities.CAT_StoreCurrency.SingleOrDefault(x => x.StoreCurrency == ToCurrencyTextBox.Text);
                        if (storeCurrency == null)
                            return;

                        storeCurrency.ExchangeRate = (float)exchange.ExchangeRate;

                        entities.SaveChanges();

                        DBValueLabel.Text = exchange.ExchangeRate.ToString(CultureInfo.InvariantCulture);
                    }

                    LastUpdateLabel.Text = $@"{DateTime.Now:dd MMM yyyy HH:mm}";
                    RateLabel.Text = exchange.ToString();
                    StatusLabel.Text = @"Success";
                    Settings.Instance.Save();
                }
                catch (Exception ex)
                {
                    StatusLabel.Text = ex.Message;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                timer.Enabled = false;
                DoUpdate();
            }
            finally
            {
                timer.Enabled = true;
            }
        }

        private void TestButton_Click(object sender, EventArgs e)
        {
            CurrencyExchange exchange = new CurrencyExchange(FromCurrencyTextBox.Text, ToCurrencyTextBox.Text);
            exchange.Refresh();

            if (Math.Abs(exchange.ExchangeRate) <= 0)
                throw new Exception("Invalid rate returned");

            MessageBox.Show(exchange.ToString());
        }
    }
}
