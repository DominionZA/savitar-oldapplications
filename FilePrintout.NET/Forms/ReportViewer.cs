﻿using System;
using System.Windows.Forms;

namespace FilePrintout.NET.Forms
{
    public partial class ReportViewer : Form
    {
        public ReportViewer(Classes.ReportItemList dataSource)
        {
            InitializeComponent();

            rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("ReportItemList", dataSource));
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {

            rv.RefreshReport();
        }
    }
}