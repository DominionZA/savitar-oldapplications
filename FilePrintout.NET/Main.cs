﻿using System.ComponentModel;
using System.Windows.Forms;

namespace FilePrintout.NET
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

#if DEBUG
            var movies = new Classes.RootFolder(@"\\HTPC-PC\Video01\Movies") {IncludeFiles = true};
            rootFoldersBindingSource.Add(movies);
            rootFoldersBindingSource.Add(new Classes.RootFolder(@"\\HTPC-PC\Video01\Series"));
#endif
        }

        private void AddFolderLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK)
                return;

            rootFoldersBindingSource.Add(new Classes.RootFolder(folderBrowserDialog.SelectedPath));
            //DS.Add(folderBrowserDialog.SelectedPath);      
        }

        private void rootFoldersBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            if ((rootFoldersBindingSource.List == null) || (rootFoldersBindingSource.List.Count == 0))
                PrintLabel.Enabled = false;
            else
                PrintLabel.Enabled = true;
        }

        private void PrintLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var frmRv = new Forms.ReportViewer(GetReportItems());
            frmRv.ShowDialog();
        }

        private void AddReportItem(Classes.ReportItemList items, string title, string folder, string[] files)
        {
            Classes.ReportItem item = new Classes.ReportItem {Title = title, Folder = folder};

            if (files != null)
            {
                item.Files = "";
                foreach (var file in files)
                {
                    if (!string.IsNullOrEmpty(item.Files))
                        item.Files += ", ";

                    item.Files += System.IO.Path.GetFileNameWithoutExtension(file);
                }
            }

            items.Add(item);
        }

        private Classes.ReportItemList GetReportItems()
        {
            Classes.ReportItemList reportItemList = new Classes.ReportItemList();

            foreach (Classes.RootFolder item in rootFoldersBindingSource.List)
            {
                Populate(reportItemList, item.Title, item.Folder, item.IncludeFiles);

                if (!item.IncludeFiles) 
                    continue;

                var files = System.IO.Directory.GetFiles(item.Folder);
                if (files.Length > 0)
                    AddReportItem(reportItemList, item.Title, "", files);
            }

            return reportItemList;
        }

        private void Populate(Classes.ReportItemList items, string category, string rootFolder, bool includeFiles)
        {
            // Get a list of folders in this item.
            var folders = System.IO.Directory.GetDirectories(rootFolder);

            foreach (var folder in folders)
            {
                if (includeFiles)
                {
                    var files = System.IO.Directory.GetFiles(folder);
                    AddReportItem(items, category, folder.Replace(rootFolder, ""), files);

                    Populate(items, category, folder, true);
                }
                else
                {
                    var subFolders = System.IO.Directory.GetDirectories(folder);
                    AddReportItem(items, category, folder.Replace(rootFolder, ""), subFolders);
                }
            }
        }
    }
}
