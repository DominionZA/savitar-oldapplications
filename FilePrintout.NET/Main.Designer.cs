﻿namespace FilePrintout.NET
{
  partial class Main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.grid = new System.Windows.Forms.DataGridView();
      this.rootFoldersBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.AddFolderLabel = new System.Windows.Forms.LinkLabel();
      this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
      this.PrintLabel = new System.Windows.Forms.LinkLabel();
      this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.folderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.includeFilesDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.rootFoldersBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // grid
      // 
      this.grid.AllowUserToAddRows = false;
      this.grid.AllowUserToDeleteRows = false;
      this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.grid.AutoGenerateColumns = false;
      this.grid.BackgroundColor = System.Drawing.Color.White;
      this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.folderDataGridViewTextBoxColumn,
            this.includeFilesDataGridViewCheckBoxColumn});
      this.grid.DataSource = this.rootFoldersBindingSource;
      this.grid.Location = new System.Drawing.Point(12, 12);
      this.grid.Name = "grid";
      this.grid.RowHeadersVisible = false;
      this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
      this.grid.Size = new System.Drawing.Size(563, 334);
      this.grid.TabIndex = 0;
      // 
      // rootFoldersBindingSource
      // 
      this.rootFoldersBindingSource.DataSource = typeof(FilePrintout.NET.Classes.RootFolders);
      this.rootFoldersBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.rootFoldersBindingSource_ListChanged);
      // 
      // AddFolderLabel
      // 
      this.AddFolderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.AddFolderLabel.AutoSize = true;
      this.AddFolderLabel.Location = new System.Drawing.Point(581, 12);
      this.AddFolderLabel.Name = "AddFolderLabel";
      this.AddFolderLabel.Size = new System.Drawing.Size(58, 13);
      this.AddFolderLabel.TabIndex = 1;
      this.AddFolderLabel.TabStop = true;
      this.AddFolderLabel.Text = "Add Folder";
      this.AddFolderLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AddFolderLabel_LinkClicked);
      // 
      // PrintLabel
      // 
      this.PrintLabel.AutoSize = true;
      this.PrintLabel.Location = new System.Drawing.Point(581, 39);
      this.PrintLabel.Name = "PrintLabel";
      this.PrintLabel.Size = new System.Drawing.Size(28, 13);
      this.PrintLabel.TabIndex = 2;
      this.PrintLabel.TabStop = true;
      this.PrintLabel.Text = "Print";
      this.PrintLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.PrintLabel_LinkClicked);
      // 
      // Title
      // 
      this.Title.DataPropertyName = "Title";
      this.Title.HeaderText = "Title";
      this.Title.Name = "Title";
      // 
      // folderDataGridViewTextBoxColumn
      // 
      this.folderDataGridViewTextBoxColumn.DataPropertyName = "Folder";
      this.folderDataGridViewTextBoxColumn.HeaderText = "Root Folder";
      this.folderDataGridViewTextBoxColumn.Name = "folderDataGridViewTextBoxColumn";
      this.folderDataGridViewTextBoxColumn.ReadOnly = true;
      this.folderDataGridViewTextBoxColumn.Width = 350;
      // 
      // includeFilesDataGridViewCheckBoxColumn
      // 
      this.includeFilesDataGridViewCheckBoxColumn.DataPropertyName = "IncludeFiles";
      this.includeFilesDataGridViewCheckBoxColumn.HeaderText = "Include Files";
      this.includeFilesDataGridViewCheckBoxColumn.Name = "includeFilesDataGridViewCheckBoxColumn";
      this.includeFilesDataGridViewCheckBoxColumn.Width = 80;
      // 
      // Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(651, 358);
      this.Controls.Add(this.PrintLabel);
      this.Controls.Add(this.AddFolderLabel);
      this.Controls.Add(this.grid);
      this.Name = "Main";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "FilePrintout.NET";
      ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.rootFoldersBindingSource)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView grid;
    private System.Windows.Forms.LinkLabel AddFolderLabel;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    private System.Windows.Forms.BindingSource rootFoldersBindingSource;
    private System.Windows.Forms.LinkLabel PrintLabel;
    private System.Windows.Forms.DataGridViewTextBoxColumn Title;
    private System.Windows.Forms.DataGridViewTextBoxColumn folderDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn includeFilesDataGridViewCheckBoxColumn;

  }
}

