﻿namespace FilePrintout.NET.Classes
{
    public class ReportItem
    {
        public string Title { get; set; }
        public string Folder { get; set; }
        public string Files { get; set; }
    }
}