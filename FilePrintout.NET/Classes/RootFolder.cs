﻿using System;
using System.ComponentModel;

namespace FilePrintout.NET.Classes
{
    public class RootFolder
    {
        public RootFolder(string rootFolder)
        {
            if (string.IsNullOrEmpty(rootFolder))
                throw new Exception("No path passed into RootFolder");

            if (rootFolder[rootFolder.Length - 1] != '\\')
                rootFolder += "\\";

            Folder = rootFolder;

            // Now try to set a default Title from the last folder      
            // First remove the trailing \
            var folder = rootFolder.Substring(0, rootFolder.Length - 1);
            // Locate the last \ now
            var index = folder.LastIndexOf('\\');
            Title = index == -1 ? "<NOT SET>" : folder.Substring(index + 1, folder.Length - (index + 1));

            IncludeFiles = false;
        }

        [DisplayName("Title")]
        public string Title { get; set; }
        [DisplayName("Root Folder"), ReadOnly(true)]
        public string Folder { get; set; }
        [DisplayName("Include Files")]
        public bool IncludeFiles { get; set; }

        public override string ToString()
        {
            return Folder;
        }
    }
}