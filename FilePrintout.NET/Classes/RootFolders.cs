﻿using System.Collections.Generic;

namespace FilePrintout.NET.Classes
{
    public class RootFolders : List<RootFolder>
    {
        public RootFolder Add(string fullPath)
        {
            var item = new RootFolder(fullPath);
            Add(item);
            return item;
        }
    }
}