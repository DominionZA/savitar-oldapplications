﻿using System;
using System.Data.OleDb;
using System.Windows.Forms;

namespace AccessQA
{
    public partial class DataSource : Form
    {
        public Classes.DataSource DS
        {
            get
            {
                if (_ds == null)
                    _ds = new Classes.DataSource();

                _ds.Name = DSNameTextBox.Text;
                _ds.File = MDBFileLabel.Text;
                _ds.UserId = UserIDTextBox.Text;
                _ds.Password = PasswordTextBox.Text;

                return _ds;
            }
            set
            {
                _ds = value;
                if (_ds != null)
                {
                    DSNameTextBox.Text = _ds.Name;
                    MDBFileLabel.Text = _ds.File;
                    UserIDTextBox.Text = _ds.UserId;
                    PasswordTextBox.Text = _ds.Password;
                }
                else
                {
                    DSNameTextBox.Text = "";
                    MDBFileLabel.Text = "";
                    UserIDTextBox.Text = "";
                    PasswordTextBox.Text = "";
                }

                ValidateOk();
            }
        }

        private Classes.DataSource _ds;

        public DataSource(Classes.DataSource dataSource)
        {
            InitializeComponent();

            DS = dataSource;
        }

        private void SelectMDBFileButton_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = @"Access files (*.mdb)|*.mdb";
            openFileDialog.InitialDirectory = MDBFileLabel.Text;
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                MDBFileLabel.Text = openFileDialog.FileName;
                TestConnection();
                ValidateOk();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error Connecting", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TestConnection()
        {
            var con = new OleDbConnection
            {
                ConnectionString = Global.GetConnectionString(MDBFileLabel.Text, UserIDTextBox.Text, PasswordTextBox.Text)
            };
            con.Open();
            con.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void DataSource_Load(object sender, EventArgs e)
        {
        }

        private void ValidateOk()
        {
            if (string.IsNullOrEmpty(DSNameTextBox.Text))
            {
                OKButtonX.Enabled = false;
                return;
            }

            if (!System.IO.File.Exists(MDBFileLabel.Text))
            {
                OKButtonX.Enabled = false;
                return;
            }

            OKButtonX.Enabled = true;
        }

        private void DSNameTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateOk();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (_ds == null) // Creating a new dataSource
                {
                    // Search the existing dataSources for the name used here. Exception if already in use.
                    var ds = Global.Settings.DataSourceList.GetByName(DSNameTextBox.Text);
                    if (ds != null)
                    {
                        DSNameTextBox.Focus();
                        DSNameTextBox.SelectAll();
                        throw new Exception("The entered name is already in use. Please enter a new name");
                    }
                }
                else
                {
                    if (_ds.Name != DSNameTextBox.Text) // Changing the name. Make sure the new name does not exist.
                    {
                        var ds = Global.Settings.DataSourceList.GetByName(DSNameTextBox.Text);
                        if (ds != null)
                        {
                            DSNameTextBox.Focus();
                            DSNameTextBox.SelectAll();
                            throw new Exception("The entered name is already in use. Please enter a new name");
                        }
                    }

                    _ds.Name = DSNameTextBox.Text;
                    _ds.File = MDBFileLabel.Text;
                    _ds.UserId = UserIDTextBox.Text;
                    _ds.Password = PasswordTextBox.Text;
                }

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Global.ShowError(ex, @"Error Saving DataSource");
            }
        }

        private void ShowPasswordCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            PasswordTextBox.PasswordChar = ShowPasswordCheckBox.Checked ? '\0' : '*';
        }

    }
}
