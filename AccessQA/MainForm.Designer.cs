﻿namespace AccessQA
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.tabControl = new System.Windows.Forms.TabControl();
      this.TabControlContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.CloseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.closeAllButThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.AddDataSourceButton = new System.Windows.Forms.ToolStripButton();
      this.NewQueryButton = new System.Windows.Forms.ToolStripButton();
      this.panel1 = new System.Windows.Forms.Panel();
      this.DataSourcesListBox = new System.Windows.Forms.ListBox();
      this.DataSourcesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.AddDataSourceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.DeleteDataSourceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.NewQueryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.TabControlContextMenu.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.DataSourcesContextMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl
      // 
      this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tabControl.ContextMenuStrip = this.TabControlContextMenu;
      this.tabControl.Location = new System.Drawing.Point(129, 0);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new System.Drawing.Size(879, 634);
      this.tabControl.TabIndex = 4;
      // 
      // TabControlContextMenu
      // 
      this.TabControlContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CloseMenuItem,
            this.closeAllButThisToolStripMenuItem});
      this.TabControlContextMenu.Name = "TabControlContextMenu";
      this.TabControlContextMenu.Size = new System.Drawing.Size(167, 70);
      // 
      // CloseMenuItem
      // 
      this.CloseMenuItem.Name = "CloseMenuItem";
      this.CloseMenuItem.Size = new System.Drawing.Size(152, 22);
      this.CloseMenuItem.Text = "Close";
      this.CloseMenuItem.Click += new System.EventHandler(this.CloseTabMenuItem_Click);
      // 
      // closeAllButThisToolStripMenuItem
      // 
      this.closeAllButThisToolStripMenuItem.Name = "closeAllButThisToolStripMenuItem";
      this.closeAllButThisToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
      this.closeAllButThisToolStripMenuItem.Text = "Close All But This";
      this.closeAllButThisToolStripMenuItem.Click += new System.EventHandler(this.closeAllButThisToolStripMenuItem_Click);
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddDataSourceButton,
            this.NewQueryButton});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(1008, 25);
      this.toolStrip1.TabIndex = 5;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // AddDataSourceButton
      // 
      this.AddDataSourceButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.AddDataSourceButton.Image = global::AccessQA.Properties.Resources.NewDataSource;
      this.AddDataSourceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.AddDataSourceButton.Name = "AddDataSourceButton";
      this.AddDataSourceButton.Size = new System.Drawing.Size(23, 22);
      this.AddDataSourceButton.Text = "Add a new data source";
      this.AddDataSourceButton.Click += new System.EventHandler(this.AddDataSourceButton_Click);
      // 
      // NewQueryButton
      // 
      this.NewQueryButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.NewQueryButton.Image = global::AccessQA.Properties.Resources.Query;
      this.NewQueryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.NewQueryButton.Name = "NewQueryButton";
      this.NewQueryButton.Size = new System.Drawing.Size(23, 22);
      this.NewQueryButton.Text = "New Query Tab";
      this.NewQueryButton.Click += new System.EventHandler(this.NewQueryButton_Click);
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.Controls.Add(this.DataSourcesListBox);
      this.panel1.Controls.Add(this.tabControl);
      this.panel1.Location = new System.Drawing.Point(0, 28);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1008, 634);
      this.panel1.TabIndex = 7;
      // 
      // DataSourcesListBox
      // 
      this.DataSourcesListBox.ContextMenuStrip = this.DataSourcesContextMenu;
      this.DataSourcesListBox.Dock = System.Windows.Forms.DockStyle.Left;
      this.DataSourcesListBox.FormattingEnabled = true;
      this.DataSourcesListBox.Location = new System.Drawing.Point(0, 0);
      this.DataSourcesListBox.Name = "DataSourcesListBox";
      this.DataSourcesListBox.Size = new System.Drawing.Size(123, 634);
      this.DataSourcesListBox.TabIndex = 5;
      this.DataSourcesListBox.DoubleClick += new System.EventHandler(this.DataSourcesListBox_DoubleClick);
      // 
      // DataSourcesContextMenu
      // 
      this.DataSourcesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddDataSourceMenuItem,
            this.DeleteDataSourceMenuItem,
            this.toolStripMenuItem1,
            this.NewQueryMenuItem});
      this.DataSourcesContextMenu.Name = "DataSourcesContextMenu";
      this.DataSourcesContextMenu.Size = new System.Drawing.Size(170, 76);
      this.DataSourcesContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.DataSourcesContextMenu_Opening);
      // 
      // AddDataSourceMenuItem
      // 
      this.AddDataSourceMenuItem.Name = "AddDataSourceMenuItem";
      this.AddDataSourceMenuItem.Size = new System.Drawing.Size(169, 22);
      this.AddDataSourceMenuItem.Text = "Add Datasource";
      this.AddDataSourceMenuItem.Click += new System.EventHandler(this.AddDataSourceMenuItem_Click);
      // 
      // DeleteDataSourceMenuItem
      // 
      this.DeleteDataSourceMenuItem.Name = "DeleteDataSourceMenuItem";
      this.DeleteDataSourceMenuItem.Size = new System.Drawing.Size(169, 22);
      this.DeleteDataSourceMenuItem.Text = "Delete Datasource";
      this.DeleteDataSourceMenuItem.Click += new System.EventHandler(this.DeleteDataSourceMenuItem_Click);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(166, 6);
      // 
      // NewQueryMenuItem
      // 
      this.NewQueryMenuItem.Name = "NewQueryMenuItem";
      this.NewQueryMenuItem.Size = new System.Drawing.Size(169, 22);
      this.NewQueryMenuItem.Text = "New Query";
      this.NewQueryMenuItem.Click += new System.EventHandler(this.NewQueryMenuItem_Click);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1008, 662);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.toolStrip1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "MainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Access QA";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.TabControlContextMenu.ResumeLayout(false);
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.DataSourcesContextMenu.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl;
    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton NewQueryButton;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripButton AddDataSourceButton;
    private System.Windows.Forms.ListBox DataSourcesListBox;
    private System.Windows.Forms.ContextMenuStrip DataSourcesContextMenu;
    private System.Windows.Forms.ToolStripMenuItem AddDataSourceMenuItem;
    private System.Windows.Forms.ToolStripMenuItem DeleteDataSourceMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem NewQueryMenuItem;
    private System.Windows.Forms.ContextMenuStrip TabControlContextMenu;
    private System.Windows.Forms.ToolStripMenuItem CloseMenuItem;
    private System.Windows.Forms.ToolStripMenuItem closeAllButThisToolStripMenuItem;
  }
}

