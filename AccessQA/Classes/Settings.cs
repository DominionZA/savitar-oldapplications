﻿namespace AccessQA.Classes
{
    public class Settings
    {
        public DataSourceList DataSourceList { get; set; }
        public QueryList QueryList { get; set; }

        public Settings()
        {
            DataSourceList = new DataSourceList();
            QueryList = new QueryList();
        }
    }
}