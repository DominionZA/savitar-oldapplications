﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AccessQA.Classes
{
    [Serializable]
    public class DataSourceList : List<DataSource>
    {
        public DataSource GetByName(string name)
        {
            return this.FirstOrDefault(item => item.Name == name);
        }
    }
}