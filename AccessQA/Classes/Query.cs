﻿using System;

namespace AccessQA.Classes
{
    [Serializable]
    public class Query
    {
        public string Name { get; set; }
        public string QueryText
        {
            get => string.IsNullOrEmpty(queryText) ? "" : queryText.Replace("\n", "\n\r");
            set => queryText = value;
        }

        private string queryText = "";
    }
}