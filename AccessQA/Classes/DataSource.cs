﻿using System;

namespace AccessQA.Classes
{
    [Serializable]
    public class DataSource
    {
        public string Name { get; set; }
        public string File { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }

        public string GetConnectionString => Global.GetConnectionString(File, UserId, Password);

        public override string ToString()
        {
            return string.IsNullOrEmpty(this.Name) ? "Name Not Set" : this.Name;
        }
    }
}