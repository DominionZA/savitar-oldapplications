﻿namespace AccessQA
{
  partial class DataSource
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataSource));
      this.MDBFileLabel = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.SelectMDBFileButton = new System.Windows.Forms.Button();
      this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
      this.label2 = new System.Windows.Forms.Label();
      this.DSNameTextBox = new System.Windows.Forms.TextBox();
      this.CancelButtonX = new System.Windows.Forms.Button();
      this.OKButtonX = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.UserIDTextBox = new System.Windows.Forms.TextBox();
      this.PasswordTextBox = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.ShowPasswordCheckBox = new System.Windows.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // MDBFileLabel
      // 
      this.MDBFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.MDBFileLabel.Location = new System.Drawing.Point(79, 43);
      this.MDBFileLabel.Name = "MDBFileLabel";
      this.MDBFileLabel.Size = new System.Drawing.Size(454, 13);
      this.MDBFileLabel.TabIndex = 9;
      this.MDBFileLabel.Text = ">> Select an MDB File <<";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(12, 43);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(58, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "MDB File";
      // 
      // SelectMDBFileButton
      // 
      this.SelectMDBFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.SelectMDBFileButton.Location = new System.Drawing.Point(539, 38);
      this.SelectMDBFileButton.Name = "SelectMDBFileButton";
      this.SelectMDBFileButton.Size = new System.Drawing.Size(30, 23);
      this.SelectMDBFileButton.TabIndex = 2;
      this.SelectMDBFileButton.Text = "...";
      this.SelectMDBFileButton.UseVisualStyleBackColor = true;
      this.SelectMDBFileButton.Click += new System.EventHandler(this.SelectMDBFileButton_Click);
      // 
      // openFileDialog
      // 
      this.openFileDialog.FileName = "openFileDialog1";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(12, 15);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(39, 13);
      this.label2.TabIndex = 11;
      this.label2.Text = "Name";
      // 
      // DSNameTextBox
      // 
      this.DSNameTextBox.Location = new System.Drawing.Point(79, 12);
      this.DSNameTextBox.Name = "DSNameTextBox";
      this.DSNameTextBox.Size = new System.Drawing.Size(490, 20);
      this.DSNameTextBox.TabIndex = 1;
      this.DSNameTextBox.TextChanged += new System.EventHandler(this.DSNameTextBox_TextChanged);
      // 
      // CancelButtonX
      // 
      this.CancelButtonX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.CancelButtonX.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelButtonX.Location = new System.Drawing.Point(494, 119);
      this.CancelButtonX.Name = "CancelButtonX";
      this.CancelButtonX.Size = new System.Drawing.Size(75, 23);
      this.CancelButtonX.TabIndex = 4;
      this.CancelButtonX.Text = "Cancel";
      this.CancelButtonX.UseVisualStyleBackColor = true;
      this.CancelButtonX.Click += new System.EventHandler(this.CancelButton_Click);
      // 
      // OKButtonX
      // 
      this.OKButtonX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.OKButtonX.Location = new System.Drawing.Point(413, 119);
      this.OKButtonX.Name = "OKButtonX";
      this.OKButtonX.Size = new System.Drawing.Size(75, 23);
      this.OKButtonX.TabIndex = 3;
      this.OKButtonX.Text = "OK";
      this.OKButtonX.UseVisualStyleBackColor = true;
      this.OKButtonX.Click += new System.EventHandler(this.OKButton_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(12, 70);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(50, 13);
      this.label3.TabIndex = 12;
      this.label3.Text = "User ID";
      // 
      // UserIDTextBox
      // 
      this.UserIDTextBox.Location = new System.Drawing.Point(82, 67);
      this.UserIDTextBox.Name = "UserIDTextBox";
      this.UserIDTextBox.Size = new System.Drawing.Size(145, 20);
      this.UserIDTextBox.TabIndex = 13;
      // 
      // PasswordTextBox
      // 
      this.PasswordTextBox.Location = new System.Drawing.Point(300, 67);
      this.PasswordTextBox.Name = "PasswordTextBox";
      this.PasswordTextBox.PasswordChar = '*';
      this.PasswordTextBox.Size = new System.Drawing.Size(145, 20);
      this.PasswordTextBox.TabIndex = 15;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(233, 70);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(61, 13);
      this.label4.TabIndex = 14;
      this.label4.Text = "Password";
      // 
      // ShowPasswordCheckBox
      // 
      this.ShowPasswordCheckBox.AutoSize = true;
      this.ShowPasswordCheckBox.Location = new System.Drawing.Point(300, 93);
      this.ShowPasswordCheckBox.Name = "ShowPasswordCheckBox";
      this.ShowPasswordCheckBox.Size = new System.Drawing.Size(99, 17);
      this.ShowPasswordCheckBox.TabIndex = 16;
      this.ShowPasswordCheckBox.Text = "ShowPassword";
      this.ShowPasswordCheckBox.UseVisualStyleBackColor = true;
      this.ShowPasswordCheckBox.CheckedChanged += new System.EventHandler(this.ShowPasswordCheckBox_CheckedChanged);
      // 
      // DataSource
      // 
      this.AcceptButton = this.OKButtonX;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.CancelButtonX;
      this.ClientSize = new System.Drawing.Size(581, 154);
      this.Controls.Add(this.ShowPasswordCheckBox);
      this.Controls.Add(this.PasswordTextBox);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.UserIDTextBox);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.OKButtonX);
      this.Controls.Add(this.CancelButtonX);
      this.Controls.Add(this.DSNameTextBox);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.SelectMDBFileButton);
      this.Controls.Add(this.MDBFileLabel);
      this.Controls.Add(this.label1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "DataSource";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Data Source Editor";
      this.Load += new System.EventHandler(this.DataSource_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label MDBFileLabel;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button SelectMDBFileButton;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox DSNameTextBox;
    private System.Windows.Forms.Button CancelButtonX;
    private System.Windows.Forms.Button OKButtonX;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox UserIDTextBox;
    private System.Windows.Forms.TextBox PasswordTextBox;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.CheckBox ShowPasswordCheckBox;
  }
}