﻿namespace AccessQA.UserControls
{
  partial class QueryView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.QueryTextBox = new System.Windows.Forms.TextBox();
      this.grid = new System.Windows.Forms.DataGridView();
      this.panel1 = new System.Windows.Forms.Panel();
      this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
      this.MDBFileLabel = new System.Windows.Forms.Label();
      this.RecordCountLabel = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // QueryTextBox
      // 
      this.QueryTextBox.Dock = System.Windows.Forms.DockStyle.Top;
      this.QueryTextBox.Location = new System.Drawing.Point(0, 0);
      this.QueryTextBox.Multiline = true;
      this.QueryTextBox.Name = "QueryTextBox";
      this.QueryTextBox.Size = new System.Drawing.Size(731, 188);
      this.QueryTextBox.TabIndex = 2;
      this.QueryTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.QueryTextBox_KeyUp);
      // 
      // grid
      // 
      this.grid.AllowUserToAddRows = false;
      this.grid.AllowUserToDeleteRows = false;
      this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.grid.BackgroundColor = System.Drawing.Color.White;
      this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grid.Location = new System.Drawing.Point(0, 194);
      this.grid.Name = "grid";
      this.grid.ReadOnly = true;
      this.grid.Size = new System.Drawing.Size(731, 237);
      this.grid.TabIndex = 3;
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.Controls.Add(this.QueryTextBox);
      this.panel1.Controls.Add(this.grid);
      this.panel1.Location = new System.Drawing.Point(0, 32);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(731, 431);
      this.panel1.TabIndex = 5;
      // 
      // openFileDialog
      // 
      this.openFileDialog.FileName = "openFileDialog1";
      // 
      // MDBFileLabel
      // 
      this.MDBFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.MDBFileLabel.Location = new System.Drawing.Point(0, 8);
      this.MDBFileLabel.Name = "MDBFileLabel";
      this.MDBFileLabel.Size = new System.Drawing.Size(734, 13);
      this.MDBFileLabel.TabIndex = 11;
      this.MDBFileLabel.Text = ">> Select an MDB File <<";
      // 
      // RecordCountLabel
      // 
      this.RecordCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.RecordCountLabel.AutoSize = true;
      this.RecordCountLabel.Location = new System.Drawing.Point(0, 470);
      this.RecordCountLabel.Name = "RecordCountLabel";
      this.RecordCountLabel.Size = new System.Drawing.Size(59, 13);
      this.RecordCountLabel.TabIndex = 12;
      this.RecordCountLabel.Text = "Records: 0";
      // 
      // QueryView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.Controls.Add(this.RecordCountLabel);
      this.Controls.Add(this.MDBFileLabel);
      this.Controls.Add(this.panel1);
      this.Name = "QueryView";
      this.Size = new System.Drawing.Size(734, 488);
      this.Load += new System.EventHandler(this.QueryView_Load);
      ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox QueryTextBox;
    private System.Windows.Forms.DataGridView grid;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
    private System.Windows.Forms.Label MDBFileLabel;
    private System.Windows.Forms.Label RecordCountLabel;
  }
}
