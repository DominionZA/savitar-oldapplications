﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace AccessQA.UserControls
{
    public partial class QueryView : UserControl
    {
        public Classes.DataSource DataSource { get; set; }

        public string QueryText => QueryTextBox.Text;

        public QueryView(Classes.DataSource dataSource, string queryText)
        {
            InitializeComponent();

            DataSource = dataSource ?? throw new Exception("DataSource must be assigned a value");
            QueryTextBox.Text = queryText;
        }

        private string GetQuery()
        {
            if (string.IsNullOrEmpty(QueryTextBox.Text))
                throw new Exception("Please enter a query to execute");

            if (!string.IsNullOrEmpty(QueryTextBox.SelectedText))
                return QueryTextBox.SelectedText;

            return QueryTextBox.Text;
        }

        private void ExecuteQuery()
        {
            OleDbConnection con = null;

            try
            {
                if (DataSource == null)
                    throw new Exception("No DataSource has been assigned to this query window");

                Cursor = Cursors.WaitCursor;

                con = new OleDbConnection(DataSource.GetConnectionString);
                con.Open();

                OleDbCommand cmd = new OleDbCommand(GetQuery(), con) {CommandType = CommandType.Text};

                var da = new OleDbDataAdapter(cmd);
                var employees = new DataTable();
                da.Fill(employees);

                grid.DataSource = employees;
                RecordCountLabel.Text = $@"Records: {grid.Rows.Count}";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Query Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                con?.Close();

                Cursor = Cursors.Default;
            }
        }

        private void QueryView_Load(object sender, EventArgs e)
        {
            grid.AutoGenerateColumns = true;

#if DEBUG
            //if (String.IsNullOrEmpty(settings.MDBFile))
            //  settings.MDBFile = @"C:\Program Files\PicData2000\PTA Sector 13 MASTER\PTA Sector13_2000.mdb";
            //if (String.IsNullOrEmpty(settings.LastQuery)) 
            //  settings.LastQuery = "SELECT * FROM Inspections";
#endif

            MDBFileLabel.Text = DataSource.File;

            QueryTextBox.Focus();
            QueryTextBox.SelectAll();
        }

        private void QueryTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
                ExecuteQuery();
            else if (e.Modifiers == Keys.Control)
            {
                if (e.KeyCode == Keys.A)
                    QueryTextBox.SelectAll();
            }
        }
    }
}
