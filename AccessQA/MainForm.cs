﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace AccessQA
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();      
    }

    private void MainForm_Load(object sender, EventArgs e)
    {      
      RefreshDataSources();

      LoadSettings();
    }

    private void LoadSettings()
    {
      foreach (Classes.Query item in Global.Settings.QueryList)
      {
        Classes.DataSource ds = Global.Settings.DataSourceList.GetByName(item.Name);
        if (ds != null)
          CreateNewTab(ds, item.QueryText);
      }
    }

    private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      Global.Settings.QueryList.Clear();

      foreach (TabPage page in tabControl.TabPages)
      {
        UserControls.QueryView queryView = (UserControls.QueryView)page.Controls[0];

        Classes.Query query = new Classes.Query();
        query.Name = queryView.DataSource.Name;
        query.QueryText = queryView.QueryText;
        Global.Settings.QueryList.Add(query);
      }

      Global.SaveSettings();

      //settings.LastQuery = QueryTextBox.Text;
      //settings.MDBFile = MDBFileLabel.Text;
      //Savitar.Object.Save("Settings.dat", settings);
    }

    private void NewQueryButton_Click(object sender, EventArgs e)
    {
      CreateNewTab();
    }

    private void CreateNewTab(Classes.DataSource ds, string queryText)
    {
      try
      {
        TabPage page = new TabPage();
        page.Text = ds.Name;
        UserControls.QueryView queryView = new UserControls.QueryView(ds, queryText);
        queryView.Dock = DockStyle.Fill;

        page.Controls.Add(queryView);
        
        tabControl.TabPages.Add(page);        
      }
      catch (Exception ex)
      {
        Global.ShowError(ex, "Failed to add query");
      }
    }

    private void CreateNewTab()
    {
      try
      {
        if ((DataSourcesListBox.SelectedItems == null) || (DataSourcesListBox.SelectedItems.Count == 0))
          throw new Exception("Please select a datasource for the new query");

        foreach (Classes.DataSource ds in DataSourcesListBox.SelectedItems)
          CreateNewTab(ds, "");
      }
      catch (Exception ex)
      {
        Global.ShowError(ex, "Failed to add query");
      }
    }

    private void EditDataSource(Classes.DataSource datasource)
    {
      DataSource frm = new DataSource(datasource);
      if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
        return;

      if (datasource == null)
        Global.Settings.DataSourceList.Add(frm.DS);

      RefreshDataSources();
    }

    private void AddDataSourceButton_Click(object sender, EventArgs e)
    {
      EditDataSource(null);
    }

    private void RefreshDataSources()
    {
      List<Classes.DataSource> items = (
        from item in Global.Settings.DataSourceList
        orderby item.Name
        select item).ToList();

      DataSourcesListBox.Items.Clear();
      foreach (Classes.DataSource item in items)
        DataSourcesListBox.Items.Add(item);

      if (DataSourcesListBox.Items.Count > 0)
        DataSourcesListBox.SelectedIndex = 0;
    }

    private void AddDataSourceMenuItem_Click(object sender, EventArgs e)
    {
      EditDataSource(null);
    }

    private void DataSourcesContextMenu_Opening(object sender, CancelEventArgs e)
    {
      if ((DataSourcesListBox.SelectedItems == null) || (DataSourcesListBox.SelectedItems.Count == 0))
      {
        DeleteDataSourceMenuItem.Enabled = false;
        NewQueryMenuItem.Enabled = false;
      }
      else
      {
        DeleteDataSourceMenuItem.Enabled = true;
        NewQueryMenuItem.Enabled = true;
      }
    }

    private void DeleteDataSourceMenuItem_Click(object sender, EventArgs e)
    {
      if (MessageBox.Show("Are you sure you want to delete the selected data source(s)?", "Confirm Delete", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
        return;

      foreach (Classes.DataSource ds in DataSourcesListBox.SelectedItems)
        Global.Settings.DataSourceList.Remove(ds);

      RefreshDataSources();
    }

    private void DataSourcesListBox_DoubleClick(object sender, EventArgs e)
    {
      if ((DataSourcesListBox.SelectedItems == null) || (DataSourcesListBox.SelectedItems.Count == 0))
        return;
      
      Classes.DataSource ds = (Classes.DataSource)DataSourcesListBox.SelectedItems[0];

      string oldName = ds.Name;

      EditDataSource(ds);

      if (oldName != ds.Name) // Name has changed. Rename all tabs from oldName to newName
      {
        foreach (TabPage page in tabControl.TabPages)
        {
          if (page.Text == oldName)
            page.Text = ds.Name;
        }
      }
    }

    private void NewQueryMenuItem_Click(object sender, EventArgs e)
    {
      CreateNewTab();      
    }

    private void CloseTabMenuItem_Click(object sender, EventArgs e)
    {
      if (tabControl.SelectedTab == null)
        return;

      if (MessageBox.Show("Are you sure you want to close the current active tab?", "Confirm Close", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
        return;

      tabControl.TabPages.Remove(tabControl.SelectedTab);
    }

    private void closeAllButThisToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (MessageBox.Show("Are you sure you want to close all tabs except the currently selected tab?", "Confirm Close", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
        return;

      for (int index = tabControl.TabPages.Count - 1; index > 0; index--)
      {
        if (tabControl.TabPages[index] != tabControl.SelectedTab)
          tabControl.TabPages.RemoveAt(index);
      }

    }
  }
}
