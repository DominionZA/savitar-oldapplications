﻿using System;
using System.Windows.Forms;

namespace AccessQA
{
    public static class Global
    {
        public static Classes.Settings Settings;

        public static void LoadSettings()
        {
            Settings = (Classes.Settings)Savitar.Object.Load("Settings.dat", typeof(Classes.Settings)) ?? new Classes.Settings();
        }

        public static void SaveSettings()
        {
            Savitar.Object.Save("Settings.dat", Settings);
        }

        public static string GetConnectionString(string fileName, string userID, string password)
        {
            var result = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName;
            if (!string.IsNullOrEmpty(userID))
                result += ";User Id=" + userID;
            if (!string.IsNullOrEmpty(password))
                result += ";Password=" + password;

            return result;
        }

        public static void ShowError(Exception ex, string caption)
        {
            MessageBox.Show(ex.Message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowError(Exception ex)
        {
            ShowError(ex, "Error");
        }
    }
}