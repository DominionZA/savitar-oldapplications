﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace RSLinks
{
    public partial class MainForm : Form
    {
        private readonly PageDataList pageList = new PageDataList();

        public MainForm()
        {
            InitializeComponent();
            Text = @"Savitar RSLinks Scraper : " + Assembly.GetEntryAssembly()?.GetName().Version;
            
        }

        public sealed override string Text { get; set; }

        private void GetDataButton_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var pageCount = (int) PageCountUpDown.Value;
                pageList.Clear();

                for (var pageNumber = 0; pageNumber <= pageCount; pageNumber++)
                {
                    var url = String.Format(Global.MoviesURL, pageNumber);
                    pageList.Add(pageNumber, url, Tools.GetPageContents(url));

                    url = string.Format(Global.SeriesURL, pageNumber);
                    pageList.Add(pageNumber, url, Tools.GetPageContents(url));
                }

                BindRips();
                BindYears();
                BindResolutions();
                BindMovies();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }        

        protected void BindRips()
        {
            RipTypesCheckListBox.Items.Clear();

            var items = pageList.GetRips().ToArray();
            RipTypesCheckListBox.Items.AddRange(items);

            for (var i = 0; i < RipTypesCheckListBox.Items.Count; i++)
                RipTypesCheckListBox.SetItemChecked(i, true);
        }

        protected void BindYears()
        {
            YearsCheckListBox.Items.Clear();

            var items = pageList.GetYears().ToArray();
            YearsCheckListBox.Items.AddRange(items);
            for (var i = 0; i < YearsCheckListBox.Items.Count; i++)
                YearsCheckListBox.SetItemChecked(i, true);
        }

        protected void BindResolutions()
        {
            ResolutionsCheckListBox.Items.Clear();

            var items = pageList.GetResolutions().ToArray();
            ResolutionsCheckListBox.Items.AddRange(items);
            for (var i = 0; i < ResolutionsCheckListBox.Items.Count; i++)
                ResolutionsCheckListBox.SetItemChecked(i, true);
        }

        protected void BindMovies()
        {
            // <h2 class="title"><a href="/movies/brrip/everything-must-go-2011-720p-brrip-xvid-ac3-flawl3ss">Everything Must Go (2011) 720p BRRip XviD AC3 - FLAWL3SS</a></h2>
            MoviesListView.Items.Clear();
            foreach (Movie movie in pageList.GetMovies(GetSelectedStrings(RipTypesCheckListBox), GetSelectedStrings(YearsCheckListBox), GetSelectedStrings(ResolutionsCheckListBox)))
            {
                var title = movie.Title;
                if (movie.Year != 0)
                    title += " (" + movie.Year + ")";
                var item = new ListViewItem(title) {Tag = movie};


                item.SubItems.Add(movie.Year == 0 ? "" : movie.Year.ToString());
                item.SubItems.Add(movie.Rip);
                item.SubItems.Add(movie.MovieUrl);
                item.ToolTipText = movie.OriginalText;

                MoviesListView.Items.Add(item);
            }
        }

        public List<string> GetSelectedStrings(CheckedListBox checkListBox)
        {
            return checkListBox.Items.Cast<object>().Where((t, i) => checkListBox.GetItemChecked(i))
                .Select(t => t.ToString()).ToList();
        }

        private bool isRipChecking;
        private void RipsCheckListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (isRipChecking)
                return;
            
            try
            {
                isRipChecking = true;
                RipTypesCheckListBox.SetItemCheckState(e.Index, e.NewValue);

                BindMovies();
            }
            finally
            {
                isRipChecking = false;
            }
        }

        private bool isYearChecking;
        private void YearCheckListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (isYearChecking)
                return;

            try
            {
                isYearChecking = true;
                YearsCheckListBox.SetItemCheckState(e.Index, e.NewValue);

                BindMovies();
            }
            finally
            {
                isYearChecking = false;
            }
        }

        private void CopyToClipboardButton_Click(object sender, EventArgs e)
        {
            if (MoviesListView.CheckedIndices.Count == 0)
            {
                MessageBox.Show(@"No movies have been checked to copy to the clipboard", @"No selection",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;               
            }

            var sb = new StringBuilder();

            foreach (int index in MoviesListView.CheckedIndices)
            {
                var movie = (Movie) MoviesListView.Items[index].Tag;
                sb.AppendLine(movie.FullPageUrl);
            }

            Clipboard.SetText(sb.ToString());
        }

        private void MoviesListView_DoubleClick(object sender, EventArgs e)
        {
            var movie = SelectedMovie;
            if (movie == null)
            {
                ShowMessage("No movie has been selected");
                return;
            }

            System.Diagnostics.Process.Start(movie.FullPageUrl);
        }

        protected void ShowMessage(string message)
        {
            MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        protected Movie SelectedMovie
        {
            get
            {
                if (MoviesListView.SelectedItems.Count == 0)
                    return null;

                return (Movie)MoviesListView.SelectedItems[0].Tag;
            }            
        }

        private void checkAtIMDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var movie = SelectedMovie;
            if (movie == null)
            {
                ShowMessage("No movie has been selected");
                return;
            }

            var url = movie.InfoUrl;
            if (string.IsNullOrEmpty(url))
            {
                var search = movie.Title;
                if (movie.Year != 0)
                    search += "+" + movie.Year;

                url = string.Format(Global.IMDBSearchURL, search);
            }

            System.Diagnostics.Process.Start(url);
        }

        private void gridContextMenu_Opening(object sender, CancelEventArgs e)
        {
            additionalInfoToolStripMenuItem.Enabled = SelectedMovie != null;
        }

        private bool isResolutionChecking;
        private void ResolutionsCheckListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (isResolutionChecking)
                return;

            try
            {
                isResolutionChecking = true;
                ResolutionsCheckListBox.SetItemCheckState(e.Index, e.NewValue);

                BindMovies();
            }
            finally
            {
                isResolutionChecking = false;
            }
        }
    }
}