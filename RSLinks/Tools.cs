﻿using System.IO;
using System.Net;

namespace RSLinks
{
    public static class Tools
    {
        public static string GetPageContents(string url)
        {
            var webRequest = HttpWebRequest.Create(url);

            var webResponse = webRequest.GetResponse();

            var streamReader = new StreamReader(webResponse.GetResponseStream());
            try
            {
                return streamReader.ReadToEnd();
            }
            finally
            {
                streamReader.Close();
            }
        }
    }
}