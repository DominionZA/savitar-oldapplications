﻿using System;
using System.Collections.Generic;
using System.Linq;
using Corsis.Xhtml;

namespace RSLinks
{
    public class PageData
    {
        public MovieList MovieList { get; }
        public int PageNo { get; }
        public string Contents { get; }
        public string Title => null;

        public override string ToString()
        {
            return PageNo.ToString();
        }

        public PageData(string contents, int pageNo)
        {
            MovieList = new MovieList();
            Contents = contents;
            PageNo = pageNo;

            ParseData();
        }

        protected void ParseData()
        {
            var xDocument = Html2Xhtml.RunAsFilter(stdin => stdin.Write(Contents)).ReadToXDocument(keepXhtmlNamespace: true);
            xDocument.Save(@"D:\Temp\AAAA.xml");

            // Query the data and write out a subset of contacts
            var movieDivs = from element in xDocument.Descendants("div")
                            where (string)element.Attribute("class") == "node node-teaser node-type-post"
                            select element;


            string infoUrl = null;
            foreach (var movieDiv in movieDivs)
            {
                // Get the title.
                var titleElement = (from h2Element in movieDiv.Descendants("h2")
                                    from aElement in h2Element.Descendants("a")
                                    where (string)h2Element.Attribute("class") == "title"
                                    select aElement).SingleOrDefault();

                if (titleElement == null)
                    continue;

                var title = titleElement.Value;

                var movieUrl = titleElement.Attribute("href")?.Value;

                var infoElement = (from aElement in movieDiv.Descendants("a")
                                   where aElement.Attribute("title") != null && (
                                    ((string)aElement.Attribute("title")).Contains("imdb.com") ||
                                    ((string)aElement.Attribute("title")).Contains("tvdb.com")
                                    )
                                   select aElement).SingleOrDefault();

                if (infoElement != null)
                    infoUrl = infoElement.Attribute("title")?.Value;

                // Need to check if this is a series link.
                if (title.Contains("Episode"))
                {
                    var rsLinksElement = (from aElement in movieDiv.Descendants("a")
                                          where aElement.Attribute("title") != null && (
                                           (string)aElement.Attribute("title")).Contains("www.rslinks.org")
                                          select aElement).SingleOrDefault();
                    if (rsLinksElement != null)
                        movieUrl = rsLinksElement.Attribute("href")?.Value.Replace("http://www.rslinks.org", "");
                }

                var movie = new Movie(movieUrl, title, infoUrl);
                MovieList.Add(movie);
            }
        }

        //protected void ParseData()
        //{
        //    HtmlParser parse = new HtmlParser(Contents);
        //    HtmlTag tag;
        //    while (parse.ParseNext("a", out tag))
        //    {
        //        // See if this anchor links to us
        //        string url;
        //        if (tag.Attributes.TryGetValue("href", out url))
        //        {
        //            if (MovieList.PageURLExists(url))
        //                continue;

        //            URL = new PageURL(url);
        //            if (URL.IsCam)
        //                continue;
        //            if (!URL.IsMovie && !URL.IsTVSeries)
        //                continue;
        //            if (URL.IsLinkToCategory)
        //                continue;


        //            string startTag = "<a href=\"" + url + "\"";
        //            int startIndex = Contents.IndexOf(startTag);
        //            startIndex += startTag.Length;

        //            string endTag = "</a>";
        //            int endIndex = Contents.IndexOf(endTag, startIndex);

        //            startIndex++;
        //            Title = Contents.Substring(startIndex, endIndex - startIndex);

        //            Movie movie = new Movie(URL.URL, Title);
        //            MovieList.Add(movie);
        //        }
        //    }
        //}
    }

    public class PageDataList : List<PageData>
    {
        public PageData Add(int pageNo, string url, string contents)
        {
            PageData item = new PageData(contents, pageNo);
            Add(item);
            return item;
        }

        public List<Movie> GetMovies(List<string> rips, List<string> years, List<string> resolutions)
        {
            var qry = (
                          from pageData in this
                          from movie in pageData.MovieList
                          orderby movie.Title, movie.Rip
                          select movie
                      );

            if (rips != null && rips.Count > 0)
                qry = (
                          from item in qry
                          join rip in rips on item.Rip equals rip
                          select item
                      );

            if (years != null && years.Count != 0)
                qry = (
                          from item in qry
                          join year in years on item.Year.ToString() equals year
                          select item
                      );

            if (resolutions != null && resolutions.Count != 0)
                qry = (
                          from item in qry
                          join resolution in resolutions on item.Resolution equals resolution
                          select item
                      );

            return qry.OrderBy(x => x.Title).ThenBy(x => x.Rip).ToList();
        }

        public List<string> GetRips()
        {
            return (
                       from pageData in this
                       from movie in pageData.MovieList
                       where !String.IsNullOrEmpty(movie.Rip)
                       select movie.Rip
                   ).Distinct().OrderBy(x => x).ToList();
        }

        public List<string> GetYears()
        {
            return (
                       from pageData in this
                       from movie in pageData.MovieList
                       select movie.Year.ToString()
                   ).Distinct().OrderBy(x => x).ToList();
        }

        public List<string> GetResolutions()
        {
            return (
                       from pageData in this
                       from movie in pageData.MovieList
                       select movie.Resolution
                   ).Distinct().OrderBy(x => x).ToList();
        }
    }
}
