﻿namespace RSLinks
{
    public class PageUrl
    {
        public string Url { get; }
        public bool IsCam { get; private set; }
        public bool IsMovie { get; private set; }
        public bool IsTvSeries { get; private set; }
        public bool Is1080P { get; private set; }
        public bool Is720P { get; private set; }
        public bool IsLinkToCategory { get; private set; }

        public PageUrl(string url)
        {
            Url = url;

            ParseData();
        }

        public void ParseData()
        {
            IsCam = Url.Contains("/cam");
            IsMovie = Url.Contains("/movies");
            IsTvSeries = Url.Contains("/tv-series");
            Is1080P = Url.Contains("1080");
            Is720P = Url.Contains("1080");
            
            var slashes = Url.Split('/');
            IsLinkToCategory = slashes.Length <= 3;
        }
    }
}
