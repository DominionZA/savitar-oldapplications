﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSLinks
{
    public class Global
    {
        public static string MoviesURL
        {
            get { return "http://www.rslinks.org/movies?page={0}"; }
        }

        public static string SeriesURL
        {
            get { return "http://www.rslinks.org/tv-series?page={0}"; }
        }

        // Replace {0} with the search string "TEST01+TEST02+2012"
        public static string IMDBSearchURL
        {
            get { return "http://www.imdb.com/find?q={0}&s=all"; }
        }
    }
}
