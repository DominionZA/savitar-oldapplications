﻿namespace RSLinks
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GetDataButton = new System.Windows.Forms.Button();
            this.PageCountUpDown = new System.Windows.Forms.NumericUpDown();
            this.MoviesListView = new System.Windows.Forms.ListView();
            this.TitleColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.YearColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RipColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PageURLColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gridContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.additionalInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RipTypesCheckListBox = new System.Windows.Forms.CheckedListBox();
            this.YearsCheckListBox = new System.Windows.Forms.CheckedListBox();
            this.CopyToClipboardButton = new System.Windows.Forms.Button();
            this.RipTypesFilterGroupBox = new System.Windows.Forms.GroupBox();
            this.YearFilterGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ResolutionsCheckListBox = new System.Windows.Forms.CheckedListBox();
            ((System.ComponentModel.ISupportInitialize)(this.PageCountUpDown)).BeginInit();
            this.gridContextMenu.SuspendLayout();
            this.RipTypesFilterGroupBox.SuspendLayout();
            this.YearFilterGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // GetDataButton
            // 
            this.GetDataButton.Location = new System.Drawing.Point(167, 12);
            this.GetDataButton.Name = "GetDataButton";
            this.GetDataButton.Size = new System.Drawing.Size(75, 23);
            this.GetDataButton.TabIndex = 0;
            this.GetDataButton.Text = "Search";
            this.GetDataButton.UseVisualStyleBackColor = true;
            this.GetDataButton.Click += new System.EventHandler(this.GetDataButton_Click);
            // 
            // PageCountUpDown
            // 
            this.PageCountUpDown.Location = new System.Drawing.Point(96, 15);
            this.PageCountUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.PageCountUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PageCountUpDown.Name = "PageCountUpDown";
            this.PageCountUpDown.Size = new System.Drawing.Size(62, 20);
            this.PageCountUpDown.TabIndex = 1;
            this.PageCountUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MoviesListView
            // 
            this.MoviesListView.CheckBoxes = true;
            this.MoviesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TitleColumn,
            this.YearColumn,
            this.RipColumn,
            this.PageURLColumn});
            this.MoviesListView.ContextMenuStrip = this.gridContextMenu;
            this.MoviesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoviesListView.Location = new System.Drawing.Point(3, 16);
            this.MoviesListView.Name = "MoviesListView";
            this.MoviesListView.ShowItemToolTips = true;
            this.MoviesListView.Size = new System.Drawing.Size(763, 427);
            this.MoviesListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.MoviesListView.TabIndex = 2;
            this.MoviesListView.UseCompatibleStateImageBehavior = false;
            this.MoviesListView.View = System.Windows.Forms.View.Details;
            this.MoviesListView.DoubleClick += new System.EventHandler(this.MoviesListView_DoubleClick);
            // 
            // TitleColumn
            // 
            this.TitleColumn.Text = "Title";
            this.TitleColumn.Width = 250;
            // 
            // YearColumn
            // 
            this.YearColumn.Text = "Year";
            // 
            // RipColumn
            // 
            this.RipColumn.Text = "Rip";
            this.RipColumn.Width = 80;
            // 
            // PageURLColumn
            // 
            this.PageURLColumn.Text = "Page URL";
            this.PageURLColumn.Width = 350;
            // 
            // gridContextMenu
            // 
            this.gridContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.additionalInfoToolStripMenuItem});
            this.gridContextMenu.Name = "gridContextMenu";
            this.gridContextMenu.Size = new System.Drawing.Size(154, 26);
            this.gridContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.gridContextMenu_Opening);
            // 
            // additionalInfoToolStripMenuItem
            // 
            this.additionalInfoToolStripMenuItem.Name = "additionalInfoToolStripMenuItem";
            this.additionalInfoToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.additionalInfoToolStripMenuItem.Text = "Additional Info";
            this.additionalInfoToolStripMenuItem.Click += new System.EventHandler(this.checkAtIMDBToolStripMenuItem_Click);
            // 
            // RipTypesCheckListBox
            // 
            this.RipTypesCheckListBox.CheckOnClick = true;
            this.RipTypesCheckListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RipTypesCheckListBox.FormattingEnabled = true;
            this.RipTypesCheckListBox.Location = new System.Drawing.Point(3, 16);
            this.RipTypesCheckListBox.Name = "RipTypesCheckListBox";
            this.RipTypesCheckListBox.Size = new System.Drawing.Size(143, 219);
            this.RipTypesCheckListBox.TabIndex = 3;
            this.RipTypesCheckListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.RipsCheckListBox_ItemCheck);
            // 
            // YearsCheckListBox
            // 
            this.YearsCheckListBox.CheckOnClick = true;
            this.YearsCheckListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.YearsCheckListBox.FormattingEnabled = true;
            this.YearsCheckListBox.Location = new System.Drawing.Point(3, 16);
            this.YearsCheckListBox.Name = "YearsCheckListBox";
            this.YearsCheckListBox.Size = new System.Drawing.Size(140, 89);
            this.YearsCheckListBox.TabIndex = 4;
            this.YearsCheckListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.YearCheckListBox_ItemCheck);
            // 
            // CopyToClipboardButton
            // 
            this.CopyToClipboardButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CopyToClipboardButton.Location = new System.Drawing.Point(821, 496);
            this.CopyToClipboardButton.Name = "CopyToClipboardButton";
            this.CopyToClipboardButton.Size = new System.Drawing.Size(115, 23);
            this.CopyToClipboardButton.TabIndex = 5;
            this.CopyToClipboardButton.Text = "Copy To Clipboard";
            this.CopyToClipboardButton.UseVisualStyleBackColor = true;
            this.CopyToClipboardButton.Click += new System.EventHandler(this.CopyToClipboardButton_Click);
            // 
            // RipTypesFilterGroupBox
            // 
            this.RipTypesFilterGroupBox.Controls.Add(this.RipTypesCheckListBox);
            this.RipTypesFilterGroupBox.Location = new System.Drawing.Point(12, 41);
            this.RipTypesFilterGroupBox.Name = "RipTypesFilterGroupBox";
            this.RipTypesFilterGroupBox.Size = new System.Drawing.Size(149, 238);
            this.RipTypesFilterGroupBox.TabIndex = 6;
            this.RipTypesFilterGroupBox.TabStop = false;
            this.RipTypesFilterGroupBox.Text = "Rip Type Filter";
            // 
            // YearFilterGroupBox
            // 
            this.YearFilterGroupBox.Controls.Add(this.YearsCheckListBox);
            this.YearFilterGroupBox.Location = new System.Drawing.Point(15, 285);
            this.YearFilterGroupBox.Name = "YearFilterGroupBox";
            this.YearFilterGroupBox.Size = new System.Drawing.Size(146, 108);
            this.YearFilterGroupBox.TabIndex = 7;
            this.YearFilterGroupBox.TabStop = false;
            this.YearFilterGroupBox.Text = "Year Filter";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MoviesListView);
            this.groupBox1.Location = new System.Drawing.Point(167, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(769, 446);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Results";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Page Count";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ResolutionsCheckListBox);
            this.groupBox2.Location = new System.Drawing.Point(18, 399);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(146, 120);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resolutions";
            // 
            // ResolutionsCheckListBox
            // 
            this.ResolutionsCheckListBox.CheckOnClick = true;
            this.ResolutionsCheckListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResolutionsCheckListBox.FormattingEnabled = true;
            this.ResolutionsCheckListBox.Location = new System.Drawing.Point(3, 16);
            this.ResolutionsCheckListBox.Name = "ResolutionsCheckListBox";
            this.ResolutionsCheckListBox.Size = new System.Drawing.Size(140, 101);
            this.ResolutionsCheckListBox.TabIndex = 4;
            this.ResolutionsCheckListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ResolutionsCheckListBox_ItemCheck);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 531);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.YearFilterGroupBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.RipTypesFilterGroupBox);
            this.Controls.Add(this.CopyToClipboardButton);
            this.Controls.Add(this.GetDataButton);
            this.Controls.Add(this.PageCountUpDown);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "www.rslinks.org";
            ((System.ComponentModel.ISupportInitialize)(this.PageCountUpDown)).EndInit();
            this.gridContextMenu.ResumeLayout(false);
            this.RipTypesFilterGroupBox.ResumeLayout(false);
            this.YearFilterGroupBox.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GetDataButton;
        private System.Windows.Forms.NumericUpDown PageCountUpDown;
        private System.Windows.Forms.ListView MoviesListView;
        private System.Windows.Forms.ColumnHeader TitleColumn;
        private System.Windows.Forms.CheckedListBox RipTypesCheckListBox;
        private System.Windows.Forms.ColumnHeader YearColumn;
        private System.Windows.Forms.CheckedListBox YearsCheckListBox;
        private System.Windows.Forms.ColumnHeader RipColumn;
        private System.Windows.Forms.ColumnHeader PageURLColumn;
        private System.Windows.Forms.Button CopyToClipboardButton;
        private System.Windows.Forms.ContextMenuStrip gridContextMenu;
        private System.Windows.Forms.ToolStripMenuItem additionalInfoToolStripMenuItem;
        private System.Windows.Forms.GroupBox RipTypesFilterGroupBox;
        private System.Windows.Forms.GroupBox YearFilterGroupBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckedListBox ResolutionsCheckListBox;
    }
}

