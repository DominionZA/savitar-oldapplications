﻿using System;

namespace RSLinks
{
    public class Movie
    {
        public enum Resolutions { Unknown, _480p, _720p, _1080p }

        public string Title { get; private set; }
        public string Rip { get; private set; }
        public string Resolution { get; private set; }
        public string InfoUrl { get; }
        public string MovieUrl { get; }
        public string FullPageUrl => "http://www.rslinks.org" + MovieUrl;
        public string OriginalText { get; private set; }
        public int Year { get; private set; }

        public Movie(string movieUrl, string description, string infoUrl)
        {            
            MovieUrl = movieUrl;
            OriginalText = description;
            InfoUrl = infoUrl;

            ParseData(description);
        }

        protected void ParseData(string description)
        {
            ExtractYear(description);
            ExtractRip();
            ExtractResolution();
        }

        protected void ExtractYear(string description)
        {
            var startIndex = description.IndexOf("(", StringComparison.Ordinal);
            var endIndex = description.IndexOf(")", StringComparison.Ordinal);
            if (startIndex == -1 || endIndex == -1)
            {
                Year = 0;
                Title = description;
                return;
            }

            int.TryParse(description.Substring(startIndex + 1, endIndex - startIndex - 1), out var year);
            Year = year;

            Title = description.Substring(0, startIndex).Trim();
        }

        protected void ExtractRip()
        {
            // Rip comes from the URL.
            var rip = MovieUrl.Replace("/movies/", "").Replace("/tv-series/", "");

            var index = rip.IndexOf("/", StringComparison.Ordinal);
            if (index == -1)
                return;

            Rip = rip.Substring(0, index);
        }

        protected void ExtractResolution()
        {
            if (MovieUrl.Contains("1080p"))
                Resolution = "1080p";
            else if (MovieUrl.Contains("720p"))
                Resolution = "720p";
            else if (MovieUrl.Contains("480p"))
                Resolution = "480p";
            else
                Resolution = "";
        }
    }
}
