﻿using System.Collections.Generic;
using System.Linq;

namespace RSLinks
{
    public class MovieList : List<Movie>
    {
        public bool PageUrlExists(string url)
        {
            return this.Count(x => x.MovieUrl == url) > 0;
        }
    }
}