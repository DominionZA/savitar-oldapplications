﻿using System;
using NUnit.Framework;

namespace RSLinks
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void UrlParseTest()
        {
            const int pageCount = 1;
            var pageList = new PageDataList();

            // Each page is a physical page at RSLinks with all movies listed. Including series which contains further links to the actual "movie" page.            
            for (var pageNumber = 0; pageNumber <= pageCount; pageNumber++)
            {
                var url = String.Format(Global.MoviesURL, pageNumber);
                pageList.Add(pageNumber, url, Tools.GetPageContents(url));
            }

            var movies = pageList.GetMovies(null, null, null);
            foreach (var movie in movies)
            {
                var title = movie.Title;
                var year = movie.Year;
            }
        }

        [Test]
        public void NewTest()
        {
            // http://corsis.sourceforge.net/Html2Xhtml/dox/
            var url = Global.SeriesURL;
            var html = Tools.GetPageContents(url);
            var pageData = new PageData(html, 0);
        }        
    }   
}
